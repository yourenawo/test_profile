#使用gevent实现协程
import gevent

def a():
    while True:
        print("----A----")
        gevent.sleep(0.5)

def b():
    while True:
        print("----B----")
        gevent.sleep(0.5)


g1 = gevent.spawn(a)
g2 = gevent.spawn(b)

g1.join()
g2.join()
