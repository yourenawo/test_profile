#一个udp协议的测试文件

from threading import Thread
from socket import *

def sendData():
    '''
    发送数据
    '''
    while True:
        data = input("<<")
        tcpObj.sendto(data.encode('gb2312'), (ip, int(port)))

def recvData():
    '''
    接收数据
    '''
    while True:
        recvInfo = tcpObj.recvfrom(1024)
        print("\n>>[%s]:%s"%(str(recvInfo[1]), recvInfo[0].decode("gb2312")))

ip = ''
port = 0
tcpObj = None

def main():

    global ip
    global port
    global tcpObj

    tcpObj = socket(AF_INET, SOCK_DGRAM)
    tcpObj.bind(("", 7890))

    ip = input("请输入对方ip:")
    port = input("请输入对方port:")

    ts = Thread(target = sendData)
    tr = Thread(target = recvData)

    ts.start()
    tr.start()

    ts.join()
    tr.join()

if __name__ == '__main__':
    main()
