from socket import *

def main():
    clientSocket = socket(AF_INET, SOCK_STREAM)
    clientSocket.connect(("10.10.10.135", 8888))
    #连接成功表示完成tcp的三次握手,如何连接失败，则会抛出异常，可以在此处进行捕获

    clientSocket.send("This is a test msg".encode())

    clientSocket.close()

if __name__ == '__main__':
    main()
