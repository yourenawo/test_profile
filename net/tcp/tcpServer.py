from socket import *

def main():

    tcpSocket = socket(AF_INET, SOCK_STREAM)
    tcpSocket.bind(("", 8888))
    tcpSocket.listen(5)

    clientSocket, clientInfo = tcpSocket.accept()

    recvData = clientSocket.recv(1024)

    print("%s:%s"%(clientInfo, recvData.decode()))

    clientSocket.close()
    tcpSocket.close()

if __name__ == '__main__':
    main()
