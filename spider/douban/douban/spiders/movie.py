# -*- coding: utf-8 -*-
import scrapy
import json
from douban.items import DoubanItem

class MovieSpider(scrapy.Spider):
    name = 'movie'
    allowed_domains = ['movie.douban.com']
    start_urls = ['https://movie.douban.com/j/chart/top_list?type=11&interval_id=100%3A90&action=&start=0&limit=200']

    def parse(self, response):

        datas = json.loads(response.text)

        items = DoubanItem()

        for data in datas:

            items["title"] = data.get("title")
            items["bd"] = data.get("types")
            items["star"] = data.get("rating")[0]
            items["date"] = data.get("release_date")
            yield items
