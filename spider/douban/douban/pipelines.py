# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html
import pymongo
import json

class DoubanPipeline(object):
    def process_item(self, item, spider):
        mongo_io = pymongo.MongoClient("mongodb://lyh:123@10.10.10.135:27017/admin")
        db = mongo_io.spider

        data = dict(item)
        db.douban.insert(data)
