#coding = utf-8
from h import browser
from bs4 import BeautifulSoup as bs

def test_douyu():
    browser.get("https://www.douyu.com/directory/all")

    while True:
        soup = bs(browser.page_source, 'lxml')

        names = soup.find_all("h3", {"class":"ellipsis"})
        numbers = soup.find_all("span", {"class":"dy-num fr"})

        for name, number in zip(names, numbers):
            print("home:"+name.get_text()+"person"+number.get_text().strip())

        if browser.page_source.find("shark-pager-disable-next") != -1:
            break

        browser.find_element_by_class_name("shark-pager-next").click()

    browser.quit()

test_douyu()
