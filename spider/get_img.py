#!/usr/bin/env python2
# -*- coding: utf-8 -*-
import requests
import json
import myrequest

def get_images():
    # 图片计数
    num = 0

    start = 0
    count = 30

    while True:

        headers = {"User-Agent":"User-Agent:Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)"}
        jsonurl = "http://lcoc.top/bizhi/api.php?cid=6&start=%s&count=%s"%(start, count)
        print(jsonurl)


        request = requests.get(jsonurl, headers = headers)

        html = request.text

        js = json.loads(html)

        items = js['data']

        if not items:
            print("hahaha")
            break

        for item in items:

            # 碰到有图片链接无法响应的情况，那就获取下一个图片
            try:
                imgurl = item['img_1600_900'].encode("utf-8")
                newurl = imgurl.replace("1600_900_85", "2560_1600_100")
                imgrequest = requests.get(newurl, headers = headers)
            except Exception as e:
                continue
            else:
                with open("/home/lyh/png/%s.jpg"%num, 'wb') as f:
                    f.write(imgrequest.content)
                    num += 1

        start += count

if __name__ == "__main__":
    main()
