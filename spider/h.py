from selenium import webdriver

from selenium.webdriver.chrome.options import Options
chrome_options = Options()
# chrome_options.add_argument('--no-sandbox')
chrome_options.add_argument('--disable-dev-shm-usage')
chrome_options.add_argument('--headless')
browser = webdriver.Chrome(chrome_options=chrome_options)
browser.maximize_window()

