import pymongo

def main():
    # mongoclient参数("mongodb://登陆用户名:密码@host:port/验证数据库")
    mongo_io = pymongo.MongoClient("mongodb://lyh:123@10.10.10.135:27017/admin")
    db = mongo_io.py3

    stu = db.stu
    # stu.insert({'name':'飞絮', 'age':'24'})
    cursor = stu.find()
    for c in cursor:
        print(c)

if __name__ == '__main__':
    main()
