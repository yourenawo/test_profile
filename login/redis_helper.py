import redis

class RedisHelper():
    def __init__(self, host, port):
        self.__redis = redis.Redis(host, port)

    def set(self, key, value):
        """docstring for set"""
        self.__redis.set(key,value)

    def get(self, key):
        """docstring for get"""
        return self.__redis.get(key)



