import redis

def main():
    """docstring for main"""

    try:
        client = redis.Redis(host='localhost', port='6379')
        pipe = client.pipeline()
        pipe.set('python','Hello World!')
        pipe.set('java','Hello')
        pipe.execute()

    except Exception as e:
        raise e

if __name__ == '__main__':
    main()
