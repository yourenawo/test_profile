# coding=utf-8
import pymysql

class Mysql(object):
    """封装mysql的基本操作"""
    def __init__(self, host, user, passwd, db):

        self.host = host
        self.user = user
        self.passwd = passwd
        self.db = db

    def open(self):
        self.conn = pymysql.connect(host=self.host, user=self.user, passwd=self.passwd, db=self.db)
        self.cur = self.conn.cursor()

    def close(self):
        self.cur.close()
        self.conn.close()

    def cud(self, sql, env):
        # change sql
        try:
            self.open()
            self.cur.execute(sql, env)
            self.conn.commit()
        except Exception as e:
            raise e
        finally:
            self.close()

    def all(self, sql, env=[]):
        # select sql
        # env a sql culumn list
        try:
            self.open()
            self.cur.execute(sql, env)
            result = self.cur.fetchall()
            return result
        except Exception as e:
            raise e
        finally:
            self.close()

