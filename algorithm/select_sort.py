import rand
import pdb
@rand.stime
def select(data: list, reverse=True) -> list:

    for i in range(0, len(data)//2+1):
        tem = i
        tem1 = len(data)-i-1
        for j in range(i, len(data)-i):
            if reverse == True:
                if data[tem] < data[j]:
                    tem = j
                if data[tem1] > data[j]:
                    tem1 = j
            else:
                if data[tem] > data[j]:
                    tem = j
                if data[tem1] < data[j]:
                    tem1 = j
        if tem == j and tem1 != i:
            data[tem], data[i] = data[i], data[tem]
            data[tem1], data[j] = data[j], data[tem1]

        if tem1 == i and tem != j:
            data[tem1], data[j] = data[j], data[tem1]
            data[tem], data[i] = data[i], data[tem]

        if tem == j and tem1 == i:
            data[tem], data[tem1] = data[tem1], data[tem]

        if tem != j and tem1 != i:
            if tem != i:
                data[tem], data[i] = data[i], data[tem]
            if tem1 != j:
                data[tem1], data[j] = data[j], data[tem1]

    return data

if __name__ == '__main__':
    for i in range(1,1000):
        # data = [1,2,3,4,5]
        data = rand.rand(0, 100, 15)
        da = data[::]
        # pdb.set_trace()
        data1 = select(data, reverse=False)
        print(rand.check(da, data1), end='')

