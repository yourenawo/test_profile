import rand

@rand.stime
def insert(data:list, reverse=True) -> list:

    for i in range(1,len(data)):
        j = i
        while True:
            if j == 0:
                break
            if reverse == True:
                if data[j] > data[j-1]:
                    data[j], data[j-1] = data[j-1], data[j]
            else:
                if data[j] < data[j-1]:
                    data[j], data[j-1] = data[j-1], data[j]
            j -= 1

    return data


if __name__ == '__main__':
    data = rand.rand(0, 100000, 10000)
    data1 = insert(data, reverse=False)
