import rand

@rand.stime
def select(data : list, reverse=True) -> list:

    for i in range(0, len(data)):
        for j in range(i+1, len(data)):
            if reverse == True:
                if data[i] < data[j]:
                    data[i], data[j] = data[j], data[i]
            else:
                if data[i] > data[j]:
                    data[i], data[j] = data[j], data[i]
    return data

if __name__ == '__main__':
    for i in range(0, 1000):
        data = rand.rand(0, 100, 15)
        da1 = data[::]
        data1 = select(data, reverse=False)
        print(rand.check(da1, data1), end='')

