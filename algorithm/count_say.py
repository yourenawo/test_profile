# 报数序列是一个整数序列，按照其中的整数的顺序进行报数，得到下一个数。其前五项如下：

# 1.     1
# 2.     11
# 3.     21
# 4.     1211
# 5.     111221

# 1 被读作  "one 1"  ("一个一") , 即 11。
# 11 被读作 "two 1s" ("两个一"）, 即 21。
# 21 被读作 "one 2",  "one 1" （"一个二" ,  "一个一") , 即 1211。

# 给定一个正整数 n（1 ≤ n ≤ 30），输出报数序列的第 n 项。
# 注意：整数顺序将表示为一个字符串。


class Solution:
    def countAndSay(self, n: int) -> str:

        if n == 1:
            return '1'

        else:
            strs = self.countAndSay(n-1)
            t = 0
            flag = 0
            snew = ''
            while True:
                i = flag
                for j in range(flag, len(strs)):
                    if strs[i] == strs[j]:
                        t += 1
                    else:
                        flag = j
                        snew += str(t)
                        snew += strs[i]
                        t = 0
                        break

                if j == len(strs)-1 and t != 0:
                    break

            snew += str(t)
            snew += strs[i]

            return snew


if __name__ == '__main__':
    a = Solution()
    strs = a.countAndSay(40)
    print(strs)
