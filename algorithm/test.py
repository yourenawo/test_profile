import rand
import bubble_sort
import insert_sort
import select_sort
import heer_sort
import merge_sort
import time


data = rand.rand(0, 100000, 10000)

print("二分选择排序用时：")
select_sort.select(data)
print("冒泡排序用时：")
bubble_sort.select(data)
print("插入排序用时：")
insert_sort.insert(data)
print("希尔排序用时：(new)")
heer_sort.heer(data)
print("希尔排序用时：(two)")
heer_sort.heer(data, reverse=False)
print("系统排序用时：")

start = time.time()
sorted(data)
end = time.time()
print(end-start)

print("归并排序:")
mst = time.time()
merge_sort.sort(data)
mstop = time.time()
print(mstop-mst)
