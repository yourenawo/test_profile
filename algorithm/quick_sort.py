import rand

def quick(data, reverse=True):

    if len(data) <= 1:
        return data
    left = []
    right = []

    base = data.pop()

    for x in data:
        if reverse==True:
            if x > base:
                left.append(x)
            else:
                right.append(x)
        else:
            if x < base:
                left.append(x)
            else:
                right.append(x)
    return quick(left, reverse)+[base]+quick(right, reverse)

if __name__ == "__main__":
    data = rand.rand(0, 50, 10)
    print(data)
    print(quick(data, reverse=False))
