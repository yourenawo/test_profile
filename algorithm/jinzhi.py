import string
# 62位向十进制转化
#  测试文件

def data_list() -> list:
    data = list(string.digits + string.ascii_letters)
    return data


def ten_to_62(ten:int, data:list) -> str:

    str62=''

    while True:
        if ten >= 62:
            yu = ten%62
            ten = ten//62
            str62 += data[yu]
        else:
            str62 += data[ten]
            break

    return str62[::-1]

def six2_to_ten(str62:str, data:list) -> str:

    su = 0
    for i in range(0, len(str62)):
        num = int(data.index(str62[i]))
        su += num*(62**(len(str62)-i-1))

    return su

def add(str1:str, str2:str) -> int:
    data = data_list()
    num1 = six2_to_ten(str1, data)
    num2 = six2_to_ten(str2, data)

    return ten_to_62(num1+num2, data)

print(add('2X', "1Y"))
print(add('ZZZ', "1"))
