import random
import time


def rand(start: int, stop: int, num: int) -> list:

    data = [random.randint(start, stop) for x in range(0, num)]

    return data

def stime(fun):
    def sfun(data, reverse=True):
        start = time.time()
        da = fun(data, reverse)
        end = time.time()
        print(end-start)
        return da
    return sfun

def check(l1:list, l2:list) -> bool:
    if sorted(l1) == l2:
        return True
    else:
        return False


if __name__ == '__main__':
    print(rand(0, 30, 20))

